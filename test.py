# python 3.6

import random
import time
import threading
import multiprocessing

from paho.mqtt import client as mqtt_client

broker = 'sns-siq-emq-dev.sso.sensormatic.com'
port = 8883
topic = "axis/test/performance"
# generate client ID with pub prefix randomly
client_id = ''
global client
iteration=10
workers=5
msg_count=0




def connect_mqtt_Thread(num):
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Connected to MQTT Broker!{client_id}")
        else:
            print(f"Failed to connect, return code %d", rc)

    global client_id
    client_id = f'python-mqtt-{random.randint(0, 10000000)}'
    client = mqtt_client.Client(client_id)
    client.username_pw_set('admin', 'fit4aking')
    client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish_Thread(client, num):
    global msg_count
    for i in range(iteration):
        msg_count +=1
        client = connect_mqtt_Thread(num)
        msg = '{"epochTime": "%s","id": "MQTT-PUB-%s","stSystemId": "123456-4","ipAddress": ' \
              '"192.168.1.139","model": "P32554-LVE","eventTime": "1644427966","interval":4} ' % (
                  str(time.time()), str(num)+"."+str(i))

        result = client.publish(topic, msg,qos=1)
        # result: [0, 1]
        status = result[0]

        if status == 0:
            print(f"Publisher {num}.{i} : Send {msg} to topic {topic} from client {client_id}")
        else:
            print(f"Failed to send message to topic {topic}")


def run_Thread(num):
    client = connect_mqtt_Thread(num)
    client.loop_start()
    publish_Thread(client, num)


def disconnect(client, userdata, rc):
    print("client disconnected ok")
    client.on_disconnect = disconnect
    client.disconnect()






if __name__ == '__main__':
    #global msg_count
    #msg_count = 100
    # run()
    #jobs = []  # list of jobs
    jobs_num = workers  # number of workers
    for i in range(jobs_num):
        multiprocessing.Process(target=run_Thread, args=(i,)).start()
        # Declare a new process and pass arguments to it
        # p1 = multiprocessing.Process(target=run_Thread, args=(i,))
        # #jobs.append(p1)
        # # Declare a new process and pass arguments to it
        # p2 = multiprocessing.Process(target=run_Thread, args=(i + 10,))
        # #jobs.append(p2)
        #
        # p3 = multiprocessing.Process(target=run_Thread, args=(i + 20,))
        # #jobs.append(p3)
        #
        # p4 = multiprocessing.Process(target=run_Thread, args=(i + 30,))
        # #jobs.append(p4)
        #
        # p5 = multiprocessing.Process(target=run_Thread, args=(i + 50,))
        # #jobs.append(p5)
        # #
        # multiprocessing.Process(target=run_Thread, args=(i + 50,)).start()
        # # p6 = multiprocessing.Process(target=run_Thread, args=(i + 60,))
        # # # jobs.append(p5)
        # #
        # # p7 = multiprocessing.Process(target=run_Thread, args=(i + 70,))
        # # # jobs.append(p5)
        # #
        # # p8 = multiprocessing.Process(target=run_Thread, args=(i + 80,))
        # # # jobs.append(p5)
        # #
        # # p9 = multiprocessing.Process(target=run_Thread, args=(i + 90,))
        # # # jobs.append(p5)
        # #
        # # p10 = multiprocessing.Process(target=run_Thread, args=(i + 100,))
        # # # jobs.append(p5)
        #
        # p1.start()  # starting workers
        # p2.start()  # starting workers
        # p3.start()  # starting workers
        # p4.start()  # starting workers
        # p5.start()  # starting workers
        # p6.start()  # starting workers
        # p7.start()  # starting workers
        # p8.start()  # starting workers
        # p9.start()  # starting workers
        # p10.start()  # starting workers
        # time.sleep(3)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)
        # disconnect(client)

        #time.sleep(10)
        # p1.terminate()
        # p2.terminate()
        # p3.terminate()
        # p4.terminate()
        # p5.terminate()

        # p1.join()
        # p2.join()
        # p3.join()
        # p4.join()
        # p5.join()
        # p6.join()
        # p7.join()
        # p8.join()
        # p9.join()
        # p10.join()


    # while(True):
    #     i=0
    #     p1 = multiprocessing.Process(target=run_Thread, args=(i,))
    #     p1.start()
    #     disconnect(client)
    #     p1.terminate()
    #     p1.join()
    #
    #     i=i+1










