from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import json
import time
import datetime
from datetime import datetime
from pytz import timezone

project_id = "sns-siq-test"
subscription_id = "siq-mqtt-topic-sub"
# Number of seconds the subscriber should listen for messages
timeout = 100000
msg_count=0

subscriber = pubsub_v1.SubscriberClient()
# The `subscription_path` method creates a fully qualified identifier
# in the form `projects/{project_id}/subscriptions/{subscription_id}`
# subscription_path = "projects/sns-siq-dev/subscriptions/integration-test-subs"
# subscription_path = "projects/sns-siq-dev/subscriptions/pubsub1-sub"
subscription_path = "projects/%s/subscriptions/%s"%(project_id,subscription_id)

import logging

# Create and configure logger
logging.basicConfig(filename="axis-to-conduit-pubsub-subscriber.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')

# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)

# subscriber.subscription_path(project_id, subscription_id)

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
    global msg_count
    #i = 1
    #print('[%s][%s][%s]' % (i, time.time(), message.data))
    # if json.loads(message.data)['id'] == 'MQTT-PUBLISHER':
    #     temp = '4'
    # #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # elif json.loads(message.data)['id'][-2:] == '-1':
    #     temp = '1'
    #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # elif json.loads(message.data)['id'][-2:] == '-2':
    #     temp = '2'
    #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # elif json.loads(message.data)['id'][-2:] == '-3':
    #     temp = '3'
    #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # elif json.loads(message.data)['id'][-2:] == '-4':
    #     temp = '4'
    #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # elif json.loads(message.data)['id'][-2:] == '-0':
    #     temp = '0'
    #     assert json.loads(message.data)['id'].find('B8:A4:4F:18:66:B5-%s' % (temp)) != -1
    #     assert json.loads(message.data)['stSystemId'].find('123456-%s' % (temp)) != -1
    #     assert json.loads(message.data)['ipAddress'].find('192.168.1.139') != -1
    #     assert json.loads(message.data)['model'].find('P3255%s-LVE' % (temp)) != -1
    #     assert json.loads(message.data)['interval'] == int(temp)
    #     print('[%s][%s][%s]' % (i, time.time(), message.data))
    # i = i + 1
    #print(message.data)
    #logger.info(message.data)
    #print('[%s][%s]' % ( time.time(), message))
    print(f"[{time.time()-float(json.loads(message.data)['epochTime'])}][{msg_count}][{datetime.now().time()}] : {message.data}")
    message.ack()
    msg_count+=1


streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
print(f"Listening for messages on {subscription_path}..\n")


# Wrap subscriber in a 'with' block to automatically call close() when done.

with subscriber:
    try:

        # When `timeout` is not set, result() will block indefinitely,
        # unless an exception is encountered first.
        streaming_pull_future.result(timeout=100000)
    except TimeoutError:
        streaming_pull_future.cancel()  # Trigger the shutdown.
        streaming_pull_future.result()  # Block until the shutdown is complete.
