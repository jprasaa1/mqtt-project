# python3.6

import random
import time
from paho.mqtt import client as mqtt_client
from datetime import datetime
import json as json
import logging


broker = 'sns-siq-emq-test.sso.sensormatic.com'
port = 8883
topic = "axis/test/performance"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
# username = 'emqx'
# password = 'public'
msg_count=0



# Create and configure logger
logging.basicConfig(filename="mqtt-subscriber.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')

# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set('admin', 'fit4aking')
    client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    print(f"Connected on Topic : {topic}")
    def on_message(client, userdata, msg):
        global msg_count
        msg_count+=1
        str=f"[{time.time()-float(json.loads(msg.payload)['epochTime'])}][{msg_count}][{datetime.now().time()}] {msg.payload} from {msg.topic} topic"
        print(str)
        logger.info(str)
    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    run()
