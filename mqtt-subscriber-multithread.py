# python3.6
import multiprocessing
import random
import time
from queue import Queue
import json as json
from datetime import datetime
msg_count=0

from paho.mqtt import client as mqtt_client

#queue = Queue(maxsize=10000)
broker = 'sns-siq-emq-dev.sso.sensormatic.com'
port = 8883
topic = "$queue/axis/heartbeat"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
# username = 'emqx'
# password = 'public'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set('admin', 'fit4aking')
    client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe_Thread(client: mqtt_client,num):

    def on_message(client, userdata, msg):
        global msg_count
        #print(f"Subscriber {num} : Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        #print(f"message : {msg.topic}")
        #queue.put(msg.topic)

        print(f"[{time.time() - float(json.loads(msg.payload)['epochTime'])}][{msg_count}][{datetime.now().time()}] {msg.payload} from {msg.topic} topic")
        msg_count += 1
    client.subscribe(topic)
    client.on_message = on_message


def run_Thread(num):
    client = connect_mqtt()
    subscribe_Thread(client,num)
    client.loop_forever()


if __name__ == '__main__':
    jobs = []  # list of jobs
    jobs_num = 5  # number of workers
    for i in range(jobs_num):
        # Declare a new process and pass arguments to it
        p1 = multiprocessing.Process(target=run_Thread, args=(i,))
        jobs.append(p1)
        # Declare a new process and pass arguments to it
        p2 = multiprocessing.Process(target=run_Thread, args=(i + 10,))
        jobs.append(p2)

        p3 = multiprocessing.Process(target=run_Thread, args=(i + 20,))
        jobs.append(p3)

        p4 = multiprocessing.Process(target=run_Thread, args=(i + 30,))
        jobs.append(p4)

        p5 = multiprocessing.Process(target=run_Thread, args=(i + 40,))
        jobs.append(p5)

        p1.start()  # starting workers
        p2.start()  # starting workers
        p3.start()  # starting workers
        p4.start()  # starting workers
        p5.start()  # starting workers
        #time.sleep(3)
        #print(f"Queue Size : {queue.qsize()}"  )
