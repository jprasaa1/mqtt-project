# python 3.6

import random
import time
import threading
import multiprocessing
from paho.mqtt import client as mqtt_client
#
# from dotenv import load_dotenv
# from pathlib import Path
# import os
#
# from configparser import ConfigParser
# file= 'config/config.ini'
# config= ConfigParser()
# config.read(file)
#
# env_path = Path('config')/config['env']['env']
# load_dotenv(dotenv_path=env_path)
#
# SECRET_KEY = os.getenv("ENV")
# DOMAIN = os.getenv("DOMAIN")
# EMAIL = os.getenv("ADMIN_EMAIL")

broker = 'sns-siq-emq-test.sso.sensormatic.com'
port = 8883
topic = "axis/test/performance"
# generate client ID with pub prefix randomly
client_id = ''
delay=0
# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    global client_id
    client_id = f'python-mqtt-{random.randint(0, 999999999)}'
    client = mqtt_client.Client(client_id)
    client.username_pw_set('admin', 'fit4aking')
    client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def publish(client):
    msg_count = 0
    while True:
        client = connect_mqtt()
        time.sleep(delay)
        #msg = f"messages: {msg_count}"
        msg = '{"epochTime": "%s","id": "TEST-MQTT-PUBLISHER-%d","stSystemId": "123456-4","ipAddress": ' \
              '"192.168.1.139","model": "P32554-LVE","eventTime": "1644427966","interval":4} ' % (
                  str(time.time()), 1)
        result = client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send {msg} to topic {topic} from client {client_id}")
        else:
            print(f"Failed to send message to topic {topic}")
        msg_count += 1


def run():
    client = connect_mqtt()
    client.loop_start()
    client = connect_mqtt()
    publish(client)
    client.loop_stop()








if __name__ == '__main__':
    run()