# python 3.6

import random
import time
import threading
import multiprocessing
from datetime import datetime

from paho.mqtt import client as mqtt_client

broker = 'sns-siq-emq-dev.sso.sensormatic.com'
port = 8883
topic = "axis/test/performance"
# generate client ID with pub prefix randomly
client_id = ''
client = None
msg_count = 18
start = None
max_iteration=1
delay=0
q=1


def connect_mqtt_Thread(num):
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Publisher : {num} Connected to MQTT Broker!")
        else:
            print(f"Publisher : {num} Failed to connect, return code %d\n", rc)

    global client_id
    # client_id = f'python-mqtt-{random.randint(0, 1000000)}'
    global client
    client = mqtt_client.Client(client_id)
    client.username_pw_set('admin', 'fit4aking')
    client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish_Thread(client, num):
    global msg_count
    global start
    start = time.time()
    for i in range(msg_count):
        #randint = random.randint(0, 1000000)
        # msg = { "deviceId":"ABCDEFG", "eventTime": time.time()}
        # msg = '{"deviceId":"ABCDEFG", "eventTime" : "' + str(time.time())[0:10] + '"}'
        msg = '{"epochTime": "%s","id": "MQTT-PUBLISHER-%d","stSystemId": "123456-4","ipAddress": ' \
              '"192.168.1.139","model": "P32554-LVE","eventTime": "1644427966","interval":4} ' % (
                  str(time.time()),i)
        result = client.publish(topic, msg, qos=q)
        time.sleep(delay)
        status = result[0]
        if status == 0:
            print(
                f"[{i}][{datetime.now().time()}] Publisher {num} : Send {msg} to topic {topic} from client {client_id}")
        else:
            print(f"Failed to send message to topic {topic}")
    end = time.time()
    diff = end - start
    print(f"Time Taken : {diff}")


def run_Thread(num):
    client = connect_mqtt_Thread(num)
    client.loop_start()
    publish_Thread(client, num)


def disconnect(client, userdata, rc):
    print("client disconnected ok")
    client.on_disconnect = disconnect
    client.disconnect()


#
# if __name__ == '__main__':
#     # run()
#     # jobs = []  # list of jobs
#     j = 1
#     jobs_num = 5000000  # number of workers
#     for i in range(0, jobs_num):
#         # Declare a new process and pass arguments to it
#         p1 = multiprocessing.Process(target=run_Thread, args=(i,))
#         # jobs.append(p1)
#         # Declare a new process and pass arguments to it
#         # p2 = multiprocessing.Process(target=run_Thread, args=(i + 1,))
#         # # jobs.append(p2)
#         #
#         # p3 = multiprocessing.Process(target=run_Thread, args=(i + 2,))
#         # # jobs.append(p3)
#         #
#         # p4 = multiprocessing.Process(target=run_Thread, args=(i + 3,))
#         # # jobs.append(p4)
#         #
#         # p5 = multiprocessing.Process(target=run_Thread, args=(i + 4,))
#         # # jobs.append(p5)
#         #
#         # p6 = multiprocessing.Process(target=run_Thread, args=(i + 60,))
#         # # jobs.append(p5)
#         #
#         # p7 = multiprocessing.Process(target=run_Thread, args=(i + 70,))
#         # # jobs.append(p5)
#         #
#         # p8 = multiprocessing.Process(target=run_Thread, args=(i + 80,))
#         # # jobs.append(p5)
#         #
#         # p9 = multiprocessing.Process(target=run_Thread, args=(i + 90,))
#         # # jobs.append(p5)
#         #
#         # p10 = multiprocessing.Process(target=run_Thread, args=(i + 100,))
#         # # jobs.append(p5)
#
#         p1.start()  # starting workers
#         # p2.start()  # starting workers
#         # p3.start()  # starting workers
#         # p4.start()  # starting workers
#         # p5.start()  # starting workers
#         # p6.start()  # starting workers
#         # p7.start()  # starting workers
#         # p8.start()  # starting workers
#         # p9.start()  # starting workers
#         # p10.start()  # starting workers
#         # time.sleep(3)
#         j = j + 5
#         disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#         # disconnect(client)
#
#         # time.sleep(10)
#         # p1.terminate()
#         # p2.terminate()
#         # p3.terminate()
#         # p4.terminate()
#         # p5.terminate()
#
#         p1.join()
#         # p2.join()
#         # p3.join()
#         # p4.join()
#         # p5.join()
#         # p6.join()
#         # p7.join()
#         # p8.join()
#         # p9.join()
#         # p10.join()
#
#     # while(True):
#     #     i=0
#     #     p1 = multiprocessing.Process(target=run_Thread, args=(i,))
#     #     p1.start()
#     #     disconnect(client)
#     #     p1.terminate()
#     #     p1.join()
#     #
#     #     i=i+1


if __name__ == '__main__':
    num = 1
    for j in range(max_iteration):
        # run_Thread(1)
        print (f"Iteration : {j}")
        client = connect_mqtt_Thread(num)
        client.loop_start()
        publish_Thread(client, num)
    client.disconnect()
