
import paho.mqtt.client as mqtt  # import the client1
import time
from queue import Queue
import json

q = Queue()
messages = []
publish_message_count=5
broker_address = "sns-siq-emq-dev.sso.sensormatic.com"
Q0S = 0
topic="axis/heartbeat"


def on_connect(client, userdata, flags, rc):
    client1.connected_flag = True
    # messages.append(m)
    # print(m)


def on_message(client1, userdata, message):
    # global messages
    m = "message received  ", str(message.payload.decode("utf-8"))
    messages.append(m)  # put messages in list
    q.put(m)  # put messages on queue
    print("message received  ", m)


def on_publish(client, userdata, mid):
    global messages
    m = "on publish callback mid " + str(mid)
    # messages.append(m)


def on_subscribe(client, userdata, mid, granted_qos):
    m = "on_subscribe callback mid " + str(mid)

def get_messages(que,i):
    i=0
    while not que.empty():
        message = que.get()
        print("queue: ", message)
        substr1="test message number ="+str(i)
        data = json.loads(message[1])
        data
        assert message[1].find(substr1) != -1
        i = i + 1


def test_sample():
    global client1
    client1 = mqtt.Client("P1")  # create new instance
    client1.username_pw_set('admin', 'admin')
    client1.on_connect = on_connect  # attach function to callback
    client1.on_message = on_message  # attach function to callback
    #client1.on_publish = on_publish  # attach function to callback
    client1.on_subscribe =on_subscribe        #attach function to callback
    time.sleep(1)
    print("connecting to broker")
    client1.connected_flag = False
    client1.connect(broker_address)  # connect to broker
    print("starting the loop")
    client1.loop_start()  # start the loop
    print("subscribing QOS=", Q0S)
    r = client1.subscribe(topic, Q0S)
    while not client1.connected_flag:
        print("waiting for connect")
        time.sleep(0.5)
    for i in range(publish_message_count):
        #print("publishing message "+str(i))
        #m = "test message number =" + str(i)
        #client1.publish(topic, m)
        time.sleep(1)
    while len(messages) > 0:
        print(messages.pop(0))
    # while not q.empty():
    #     message = q.get()
    #     print("queue: ", message)
    get_messages(q,i)
    client1.disconnect()
    client1.loop_stop()





