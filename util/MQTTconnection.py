
from util.Singleton import Singleton
from paho.mqtt import client as mqtt_client

class MQTTconnection():
    client = ''
    @staticmethod
    def getInstance():
        """Static Access Method"""
        if str(type(MQTTconnection.client)) == "<class 'paho.mqtt.client.Client'>":
            MQTTconnection()
        else:
            return MQTTconnection.__init__()

    def __init__(self):
        if str(type(MQTTconnection.client)) != "<class 'paho.mqtt.client.Client'>":
            MQTTconnection.client = self.client
        else:
            self.broker = 'sns-siq-emq-dev.sso.sensormatic.com'
            self.port = 1883
            self.topic = "python_queue"
            self.client_id = 'P1'

            def on_connect(client, userdata, flags, rc):
                if rc == 0:
                    print("Connected to MQTT Broker!")
                else:
                    print("Failed to connect, return code %d\n", rc)

            # client_id="P1"
            MQTTconnection.client = mqtt_client.Client(self.client_id)
            MQTTconnection.client.username_pw_set('admin', 'admin')
            # client.tls_set(ca_certs='C:\\Users\\abhishek.prasad\\Downloads\\consolidate.pem')
            MQTTconnection.client.on_connect = on_connect
            MQTTconnection.client.connect(self.broker, self.port)
            MQTTconnection.client = self.client
            return MQTTconnection


# main method
if __name__ == "__main__":
    # create object of Singleton Class
    obj = MQTTconnection()
    print(obj)


    # pick the instance of the class
    obj = MQTTconnection.getInstance()
    print(obj)
